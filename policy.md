# Keycloak Policy

Keycloak serves as the central authentication and authorization provider for a significant portion of our infrastructure, making it a critical application for security and maintenance.  
Unexpected service interruptions can disrupt the entire institute, while security incidents may result in unauthorized access to sensitive data or restricted actions.

This document aims to minimize such risks by limiting access to the necessary minimum and simplifying common operational procedures.

---

## Definitions

- **User**: A natural person who uses Keycloak to log into supported applications.
- **User Account**: A technical representation of a *user* in Keycloak, containing attributes such as name and email.
- **System Account**: A special account for system-to-system communication, not associated with a natural person.
- **Client**: In OpenID Connect (OIDC) terminology, a *client* refers to a service that relies on Keycloak for authentication and authorization, not a *user*.
- **Admin**: A Keycloak administrator responsible for support, maintenance, implementing new features, and ensuring uninterrupted platform operation.
- **Manager**: A staff member responsible for a resource (e.g., group, role, client, or account) who can request or approve changes to that resource.
- **IdP**: Identity Provider (e.g., LUMS, UL-AD, LIH-AD, or LifeScience Login).
- **Formal Approval**: Approval by the resource *manager* explicitly (via a signed form) or implicitly (via an electronic system designated by the *manager*, e.g., DAISY).
- **Informal Approval**: Approval by the resource *manager* via email, ServiceNow, Slack, Teams, or any other traceable communication method (not verbal).

---

## User Accounts Policy

1. A new *user account* is created by logging in through an available *IdP* without requiring *approval*.
2. Each *user account* is uniquely linked to the *IdP* used during its creation.
3. If the same *user* logs in through a different *IdP*, a new separate *user account* is created.
4. Email addresses and usernames are unique and cannot be shared between accounts.
5. *User account* details, such as username or email, can only be modified by an *admin* upon request by the *user*.
6. *User accounts* with privileged access to Keycloak must have 2FA enabled.
7. *User accounts* accessing sensitive data (as defined by Article 9 and Recitals (51)–(56) of the GDPR) must have 2FA enabled.
8. *Admins* can impersonate *user accounts* only with the *user's* explicit consent for debugging purposes.
9. *User accounts* that fail to comply with this policy will be disabled.

---

## System Account Policy

1. Every *system account* must have an active *manager*.
2. *System accounts* have non-expiring passwords and are not linked to an *IdP*.
3. If a *manager* leaves without naming a successor, their line manager assumes responsibility and may delegate it further.
4. *System accounts* are exempt from the 2FA requirements for regular accounts.
5. Privileged *system accounts* (e.g., those with access to the user database) are issued only in exceptional cases and as a last resort.
6. *System accounts* are formally reviewed annually by their *manager*.
7. Non-compliant *system accounts* will be disabled.

---

## Group & Role Policy

1. Every *group* or *role* must have an active *manager*.
2. If a *manager* leaves without naming a successor, their line manager assumes responsibility and may delegate it further.
3. Assigning an *account* to a *group* or *role* requires the formal approval of the *manager*.
4. *Groups* and *roles* can be linked to other *groups*, *roles*, or *clients* to manage access across resources and services.
5. Such associations require the formal approval of all involved *managers*.
6. Each *manager* must review assigned *accounts* and associations annually.
7. Non-compliant *groups* and *roles* will be removed.

---

## Client Policy

1. Every *client* must have an active *manager*.
2. If a *manager* leaves without naming a successor, their line manager assumes responsibility and may delegate it further.
3. The *client manager* is also responsible for all associated *client roles*.
4. A *client* must identify *accounts* using immutable user IDs (e.g., `sub`) rather than usernames or emails, as these may change.
5. The *client* is responsible for verifying the identity of its *users* using supplied *account* information or other means, as Keycloak only brokers external identities.
6. Redirect URI domains cannot contain wildcards (e.g., `https://*.uni.lu`); however, path wildcards are allowed (e.g., `https://service.lcsb.uni.lu/*`).
7. Access to realm roles or other client roles is restricted unless required.
8. *Client* application sessions are limited to a maximum of 12 hours, after which re-authentication is required.
9. ID tokens must remain within the *client* and should not be shared with other services or users.
10. Access tokens are issued for resource servers only and must not be parsed or interpreted by the *client* (Keycloak serves as a resource server via the `/userinfo` endpoint).
11. Non-compliant *clients* will be removed unless justified (e.g., third-party applications unable to comply).
